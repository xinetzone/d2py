```{include} ../README.md
```

```{toctree}
:maxdepth: 2
:hidden:

python-study/index
math
metaprogramming/index
reference/index
chaos/index
posts/index
news/index
about/index
```

# 索引和表格

* {ref}`genindex`
* {ref}`modindex`
* {ref}`search`
